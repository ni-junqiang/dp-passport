package net.chenlin.dp.ids.common.constant;

/**
 * 票据常量
 * @author zhouchenglin[yczclcn@163.com]
 */
public class TicketConst {

    /**
     * authStatus接口，在服务端未登录时，ticket前缀
     */
    public static final String AUTH_STATUS_NOT_LOGIN_PREFIX = "AST";

}
